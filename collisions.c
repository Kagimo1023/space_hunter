#include "global.h"

_Bool collision_Update(WINDOW *window, collision_type status){
  switch(status){
  case COLLISION_WALL:
    return returnBorders(window);
    break;
  case COLLISION_PLAYER:
    return returnObject(window, player.y - 1, player.x, B_METEOR) || returnObject(window, player.y - 1, player.x, B_COMET);
    break;
  case COLLISION_PLASMA:
    return returnObject(window, plasma->y - 1, plasma->x, B_METEOR);
    break;
  }

  return FALSE;
}

void checkPlasma(WINDOW *window){
  if(collision_Update(window, COLLISION_PLASMA)){
    mvaddcuscchyx(window, plasma->y, plasma->x, empty.disp_char, B_EMPTY, A_NORMAL, empty.pair);
    mvaddcuscchyx(window, plasma->y - 1, plasma->x, empty.disp_char, B_EMPTY, A_NORMAL, empty.pair);

    flash();
    beep();

    plasma->x = 0;
    plasma->y = 0;
  }
}

_Bool returnBorders(WINDOW *window){
  if (!player.x) player.x++;
  else if (player.x > (getmaxx(window) - 2)) player.x--;
  else if (player.y > (getmaxy(window) - 2)) player.y--;
  else if (player.y < (getmaxy(window) / 2)) player.y++;
  else return FALSE;

  mvwin(player.ply,getbegy(window) + player.y, getbegx(window) + player.x);
  return TRUE;
}

_Bool returnObject(WINDOW *window, int y, int x, const wchar_t *object){
  cchar_t cc;
  mvwin_wch (window, y, x, &cc);
	
  wchar_t ch[getcchar (&cc, 0, 0, 0, 0)];
  getcchar (&cc, ch, &(attr_t){ 0 }, &(short){ 0 }, 0);
	
  return !wcscmp (ch, object);	
}
