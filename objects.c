#include "global.h"

void initobjects(WINDOW *window){
  int bottom_scr = getmaxy(window) - 3;
  int center_scr = getmaxx(window) / 2;
  
  //Инициализация игрока
  player.y		     = bottom_scr;
  player.x		     = center_scr;
  
  player.HP          = 3;
  player.currentHP   = 3;
  
  player.speed       = 00011;
  
  player.ammo 		 = 3;
  player.currentAmmo = 1;
  
  player.shoot		 = FALSE;
  
  nraddcuscch (player.ply,player.disp_char, B_ROCKET_UP, A_NORMAL, player.pair);
}

void destroyObject(WINDOW *window, int y, int x){
  mvaddcuscchyx(window, y, x, empty.disp_char, B_EMPTY, A_NORMAL, empty.pair);
}

void initcchars(void){
  setcchar(&empty.disp_char,     B_EMPTY,     A_NORMAL, empty.pair,      0);
  setcchar(&player.disp_char, 	 B_ROCKET_UP, A_NORMAL, player.pair,     0);
  setcchar(&star.disp_char,	 B_STAR,      A_NORMAL, star.pair,       0);
  setcchar(&meteor.disp_char, 	 B_METEOR,    A_NORMAL, meteor.pair,     0);
  setcchar(&plasma -> disp_char, B_PLASMA,    A_NORMAL, plasma -> pair,  0);
  setcchar(&explosion.disp_char, B_EXPLOSION, A_BLINK , explosion.pair,  0);
}
