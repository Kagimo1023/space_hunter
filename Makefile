CFLAGS=-std=c17 -Wpedantic -Werror -Wformat -fno-common $$(pkg-config --cflags ncursesw)
LDFLAGS=$$(pkg-config --libs ncursesw)
OBJECTS=main.o menu.o scene.o input.o game.o objects.o graphics.o collisions.o global.o

shunter: $(OBJECTS)
	$(CC) -o shunter $(OBJECTS) $(LDFLAGS)
main.o menu.o scene.o input.o game.o objects.o graphics.o collisions.o global.o: global.h
