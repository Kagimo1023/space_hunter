#include "global.h"

_Bool staticDrawBG = TRUE;

char tbgstr[CAMERA_WIDTH] = " "; //Нужна для адекватной отрисовки бэкграунда

int cn = 0; //Переменная хранящая ширину окна. В данном случае "gmw" (Игровое окно)
int ln = 0; //Переменная хранящая длину окна. В данном случае "gmw" (Игровое окно)

int currentFrame = 0;

wchar_t *player_frames[][3] = { B_ROCKET_UP, B_ROCKET_DOWN, B_ROCKET_LEFT, B_ROCKET_RIGHT };
wchar_t healthbar[5];

static struct {
  unsigned char offset;
  wchar_t **tiles;
} unfinishedobjects[CAMERA_WIDTH];

/*Инициализация цветовых палитр.*/
void cinit_pairs(void){
    for(int i = 0; i <= 7; i++) init_pair(i, i, COLOR_BLACK);

    player.pair    = COLOR_YELLOW;
    star.pair      = COLOR_BLUE;
    plasma->pair   = COLOR_YELLOW;
    meteor.pair    = COLOR_RED;
    explosion.pair = COLOR_RED;
}

void nraddcuscch(WINDOW *window, cchar_t X, wchar_t *C, attr_t attr, int pair){
    setcchar(&X, C, attr, pair ,0);
    wadd_wch(window, &X);
}

void addcuscch(WINDOW *window, cchar_t X, wchar_t *C, attr_t attr, int pair){
    setcchar(&X, C, attr, pair ,0);
    wadd_wch(window, &X);
    //wrefresh(window);
}

void mvaddcuscchyx(WINDOW *window, int y, int x, cchar_t X, wchar_t *C, attr_t attr, int pair){
    setcchar(&X, C, attr, pair ,0);
    mvwadd_wch(window,y,x,&X);
    //wrefresh(window);
}

void display_box(WINDOW *window, int sy, int sx, char *str, attr_t attr, int pair){
  frwindow(window, ' ');
  
  wattron(window, COLOR_PAIR(pair));
  box(window,0,0);
  wattroff(window, COLOR_PAIR(pair));
  
  wattron(window, COLOR_PAIR(pair) | attr);
  mvwprintw(window, sy, sx, str);
  wattroff(window, COLOR_PAIR(pair) | attr);

  wrefresh(window);
}

void scrollGameMap(WINDOW *window, _Bool p){
  int spw = randomizer(1 ,100);
  
  int sc;
  int sx;
  
  int mx;
  
  for(int i = 0; i < getmaxx(window); i++) tbgstr[i] = ' ';
  
  if(pauseBool == TRUE)  scrollok(window, FALSE);
  if(pauseBool == FALSE) scrollok(window, TRUE);
  
  mvwprintw(window,0,0,tbgstr);
  
  for (int i = 0; i < celestialbodieslength; i++)
  {
    
    int x = randomizer(1,getmaxx(window));
    if (unfinishedobjects[x].offset) continue;
    int r = randomizer (1, 100);
    
    for (unsigned char x = 0; x < CAMERA_WIDTH; x++)
      if (unfinishedobjects[x].offset) {
        mvwaddwstr (window, 0, x, unfinishedobjects[x].tiles[--unfinishedobjects[x].offset]);
      }
      
    if (r < celestialbodies[i].frequency)
    {
      wattr_on(window, COLOR_PAIR(celestialbodies[i].pair),0);
      mvwaddwstr(window, 0, x, celestialbodies[i].tiles[celestialbodies[i].tilenumber - 1]);
      wattr_off(window, COLOR_PAIR(celestialbodies[i].pair),0);

      unfinishedobjects[x].offset = celestialbodies[i].tilenumber - 1;
      unfinishedobjects[x].tiles = celestialbodies[i].tiles;
    }
  }
  
  wscrl(window,-1);
}

void drawPlasma(WINDOW *window){
  mvwadd_wch (window, plasma->y + 1, plasma->x, &plasma->bg_char);
  if (plasma->y){
    mvwin_wch (window, plasma->y, plasma->x, &plasma->bg_char);
    mvaddcuscchyx(window, plasma->y, plasma->x, plasma->disp_char, B_PLASMA, A_NORMAL, plasma->pair);
  }
} 

//Function to draw Game User Interface
void display_GUI(WINDOW *window){
  wchar_t buf[100];
  
  frwindow(window, ' ');
  
  box(window,0,0);
  
  for(int i = 0; i < 5; i++){
    healthbar[i] = B_GUI_BLOCK_E;
  }
  
  for(int i = 0; i < player.HP; i++){
    healthbar[i] = B_GUI_BLOCK_N;
  }
  
  for(int i = 0; i < player.currentHP; i++){
    healthbar[i] = B_GUI_BLOCK_F;
  }
  
  swprintf(buf, 100, L"PLAYER: [%-10s] [%5ls]", player.name, healthbar);
  mvwaddwstr (window, 1, 2, buf);
  mvwprintw(window,2,2,"RECORD: [%3d.%03d %s]",record_mm / 1000, record_mm % 1000, "Mm");
}

//Создание бэкграунда состоящего из звёзд.
void drawGameBG(WINDOW *window, _Bool staticDBG){
    if(staticDBG == TRUE) {
        frwindow(window, ' ' | COLOR_PAIR(0));
        int sc;

        for(int i = 0; i < getmaxy(window); i++){
            cn = randomizer(1,getmaxx(window));
            mvaddcuscchyx(window, i, cn, star.disp_char, B_STAR, A_NORMAL, star.pair);
        }

        //wrefresh(window);
    } else {
        while(ln!=getmaxy(window)){
            cn = randomizer(1,getmaxx(window));
            mvaddcuscchyx(window, ln, cn, star.disp_char, B_STAR, A_NORMAL, star.pair);
            ++ln;
        }
    }
}

void frwindow(WINDOW *window, chtype sym){
    for(int i = 0; i <= getmaxy(window); ++i){
        for(int b = 0; b <= getmaxx(window); ++b){
            mvwaddch(window,i,b,sym);
        }
    }
}
