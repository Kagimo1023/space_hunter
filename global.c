#include "global.h"

static wchar_t *startiles[]   = { B_STAR };
static wchar_t *meteortiles[] = { B_METEOR };
static wchar_t *comettiles[] = { L"⠭", L"⠍", L"⠩", L"⠌", B_COMET };

struct recordsStruct records[5];
struct playerStruct player;
struct emptyStruct empty;
struct starStruct star;
struct meteorStruct meteor;
struct plasmaStruct plasma[5];
struct explosionStruct explosion;

struct celestialbody celestialbodies[] = {
  /* Star */
  {
    .frequency = 75,
    .tiles = startiles,
    .tilenumber = sizeof startiles / sizeof *startiles,
    .collision = FALSE,
    .pair = COLOR_BLUE
  },
  /* Meteor */
  {
    .frequency = 20,
    .tiles = meteortiles,
    .tilenumber = sizeof meteortiles / sizeof *meteortiles,
    .collision = TRUE,
    .pair = COLOR_RED
  },
  /* Comet */
  {
    .frequency = 20,
    .tiles = comettiles,
    .tilenumber = sizeof comettiles / sizeof *comettiles,
    .collision = TRUE,
    .pair = COLOR_GREEN
  }
};

size_t celestialbodieslength = sizeof celestialbodies / sizeof (struct celestialbody);

int record_mm = 00000;

void cursInit(void){
  initscr();
  cbreak();
  keypad(stdscr, TRUE);
  nodelay(stdscr, TRUE);
  scrollok(stdscr, TRUE);
  noecho();
  start_color();
  cinit_pairs();
  initcchars();
  recordsInit();
  
  player.ply = newwin(1,1,player.y,player.x);
  refresh();
}


void recordsInit(void){
  FILE *recordsFile;
  
  int recordsNum = 1;
  
  if((recordsFile = fopen("records","r")) != NULL){
    for(int i = 0; i < 5; i++){
      char line[20];
      do
      {
        if (!fgets (line, 20, recordsFile))
          goto close;
      }
      while (line[strlen(line) - 1] != '\n');
      int ret = sscanf(line, "%d:%*1[ ]%9[^ -]%*1[ ]%*1[-]%*1[ ]%" SCNuLEAST32, &recordsNum, records[i].player, &records[i].score);
      
      /* Skip broken records */
      if (ret != 3)
      {
        i--;
      }
    }
    
    close:
    fclose(recordsFile);
  } else {
    char str[10] = "ПУСТО";
    
    for (int i = 0; i < 5; i++){
      records[i].score = 0;
      memcpy (records[i].player, str, sizeof(str));
    }
  }
}

void recordsCompare(void){
  for(int i = 0; i < 5; i++){
    if(record_mm > records[i].score){
      for (int j = 4; j > i; j--){
        records[j].score = records[j - 1].score;
        memcpy(records[j].player, records[j - 1].player, 10);
      }
      
      records[i].score = record_mm;
      memcpy (records[i].player, player.name, 10);
      break;
    }
  }
}

void recordsSave(void){
  FILE *recordsFile;
  int recordsNum = 1;
  
  recordsFile = fopen("records","w");
  
  for(int i = 0; i < 5; i++){
    fprintf(recordsFile, "%d: %s - %" PRIuLEAST32 "\n", recordsNum++, records[i].player, records[i].score);
  }
  
  fclose(recordsFile);
}

int randomizer(int min, int max){
  return min + rand() % ((max + 1) - min);
}

int msleep(int ms){
  return usleep(ms * 1000);
}
