#include "global.h"

void pControls(WINDOW *window ,int ch){
  switch(ch){
    case KEY_RIGHT:
      ++player.x;
      break;
    case KEY_LEFT:
      --player.x;
      break;
    case KEY_UP:
      --player.y;
      break;
    case KEY_DOWN:
      ++player.y;
      break;
    case 'p':
      gamePause(window, pauseBool);
      return;
    case 'f':
      rocketshoot(window);
      return;
    case 'x':
      pauseBool = TRUE;
      return;
    default:
      return;
  }
  mvwin(player.ply,getbegy(window) + player.y, getbegx(window) + player.x);
}
