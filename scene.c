#include "global.h"

void gameOver(WINDOW *window){
  nodelay(stdscr, FALSE);
  int x = 0;
  
  while(x < 50){
    for(int b = 0; b <= 3; ++b){
      addcuscch(player.ply, player.disp_char, player_frames[0][b], A_NORMAL, COLOR_YELLOW);
      wrefresh (player.ply);
      usleep(6000);
    }
    ++x;
  }
  
  addcuscch(player.ply, explosion.disp_char, B_EXPLOSION, A_BLINK, explosion.pair);
  wrefresh (player.ply);
  
  sleep(4);
  
  gameOverBoxUI(window);
  
  plasma->y = 0;
  
  recordsCompare();
  recordsSave();
}

void recordsBoxUI(WINDOW *window){
  const int m_height = 11;
  const int m_width  = 35;
  
  const int y_screen = (getmaxy(window) / 2) - m_height / 2;
  const int x_screen = (getmaxx(window) / 2) - m_width / 2;
  
  char recordsTitle[] = "ТАБЛИЦА РЕКОРДОВ";
  
  int recordY = 4;
  int recordNumber = 1;
  
  WINDOW *recordsBox = newwin(m_height, m_width, y_screen, x_screen);
  
  frwindow(recordsBox, ' ');
  
  box(recordsBox,0,0);
  
  mvwprintw(recordsBox,2, (strlen(recordsTitle)) / 3, recordsTitle);
  
  for(int i = 0; i < 5; i++){
    mvwprintw(recordsBox, recordY++, 2, "%d: %-10s - %11d", recordNumber++, records[i].player, records[i].score);
  }
  
  wrefresh(recordsBox);
  wgetch(recordsBox);
  
  wclear(recordsBox);
  delwin(recordsBox);
}

void gameOverBoxUI(WINDOW *window){	
  const int m_height = 5;
  const int m_width  = 35;
  
  const int y_screen = (getmaxy(window) / 2) - m_height / 2;
  const int x_screen = (getmaxx(window) / 2) - m_width / 2;
  
  char goMsg[] = "ИГРА ОКОНЧЕНА"; 
  
  WINDOW *gameOverBox = derwin(window,m_height, m_width, y_screen, x_screen);
  
  display_box(gameOverBox, m_height / 2, 10, goMsg, A_BLINK, COLOR_PAIR(0));
  
  wgetch(window);
  
  wclear(gameOverBox);
  delwin(gameOverBox);
}

void gamePause(WINDOW *window, _Bool p){
  int ch;
  
  p 			= TRUE;
  scrollBool	= FALSE;
  
  while(p == TRUE){
    ch = getch();
    
    addcuscch(player.ply, player.disp_char, B_ROCKET_UP, A_BLINK, player.pair);
    
    nodelay(stdscr, FALSE);
    
    pauseBoxUI(window, p);
    
    if(ch == 'p'){
      p 			= FALSE;
      scrollBool	= TRUE;
      
      pauseBoxUI(window, p);
      
      nodelay(stdscr, TRUE);
      
      addcuscch(player.ply, player.disp_char, B_ROCKET_UP, A_NORMAL, player.pair);
      
      wrefresh(window);
    }
  }
}

void pauseBoxUI(WINDOW *window, _Bool p){
  const int m_height = 5;
  const int m_width  = 15;
  
  char pauseMsg[] = "ПАУЗА";
  
  WINDOW *pauseBox = newwin(m_height, m_width,(getmaxy(stdscr) / 2) - m_height / 2, (getmaxx(stdscr) / 2) - m_width / 2);

  display_box(pauseBox, m_height / 2, 5, pauseMsg, A_BLINK, COLOR_PAIR(0));
  
  if(!p) { wclear(pauseBox); delwin(pauseBox); }
}

void enterName(void){
  const int m_height = 7;
  const int m_width  = 30;
  
  const char enameTitle[] = "ИМЯ ПИЛОТА";
  const char enameMsg[]	= "НАЗОВИСЬ: ";
  
  echo();
  
  nodelay(stdscr, FALSE);
  
  curs_set(1);
  
  WINDOW *nameBox   = newwin(m_height, m_width, (getmaxy(stdscr) / 2) - m_height / 2, (getmaxx(stdscr) / 2) - m_width / 2);
  
  frwindow(nameBox, ' ' | COLOR_PAIR(0));
  
  wattron(nameBox, COLOR_PAIR(7));
  box(nameBox,0,0);
  wattroff(nameBox, COLOR_PAIR(7));
  
  wattron(nameBox, COLOR_PAIR(7));
  mvwprintw(nameBox,2, strlen(enameTitle) / 2, enameTitle);
  mvwprintw(nameBox,4, 3, enameMsg);
  wattroff(nameBox, COLOR_PAIR(7));
  
  wattron(nameBox, COLOR_PAIR(7));
  mvwgetnstr(nameBox, 4, strlen(enameMsg) - 4,player.name,10);
  wattroff(nameBox, COLOR_PAIR(7));
  
  wrefresh(nameBox);
  refresh();
  
  delwin(nameBox);
  
  curs_set(0);
  
  nodelay(stdscr, TRUE);
  
  noecho();
  
  gameLoop();
}
