#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <ncurses.h>
#include <unistd.h>
#include <inttypes.h>
#include <time.h>

#define GAME_TITLE "SPACE HUNTER"

// #define DIFFICULT_1		3
// #define DIFFICULT_2		6
// #define DIFFICULT_3		9
// #define DIFFICULT_4		12
// #define DIFFICULT_5		15

#define CAMERA_HEIGHT	16
#define CAMERA_WIDTH	55

#define GAME_SPEED      25000

#define B_GUI_BLOCK_E L'░'
#define B_GUI_BLOCK_N L'▒'
#define B_GUI_BLOCK_F L'█'

#define B_ROCKET_UP     L"⮝" //⮝⮟⮜⮞
#define B_ROCKET_DOWN   L"⮟"
#define B_ROCKET_LEFT   L"⮜"
#define B_ROCKET_RIGHT  L"⮞"

#define B_EMPTY         L" "
#define B_STAR          L"✶"
#define B_PLASMA        L"|"
#define B_METEOR        L"O"
#define B_COMET         L"ᴑ"
#define B_EXPLOSION     L"X"

extern struct recordsStruct {
  uint_least32_t score; // up to ~42 million in 1/100 fixed point
  char player[10];
} records[5];

extern struct celestialbody {
  unsigned char frequency;
  int tilenumber;
  unsigned int pair;
  _Bool collision;
  wchar_t **tiles;
} celestialbodies[];

extern size_t celestialbodieslength;

extern struct emptyStruct {
  int pair;
  cchar_t disp_char;
} empty;

extern struct starStruct {
  int pair;			//Цветовая палитра
  cchar_t disp_char;	//Символ звезды
} star;

extern struct meteorStruct {
  int pair;			//Цветовая палитра
  cchar_t disp_char;	//Символ метеорита
} meteor;

extern struct explosionStruct {
  int pair;			//Цветовая палитра
  cchar_t disp_char;	//Символ взрыва
} explosion;

extern struct playerStruct {
  WINDOW *ply;		//Окно для игрока
  char name[10];		//Имя игрока
  int y,x;			//Координаты игрока
  int pair;			//Цветовая палитра игрока
  int currentAmmo;	//Текущее число патронов
  int ammo;			//Максимальное число патронов
  int HP;           //Максимальное кол-во жизней
  int currentHP;    //Текущее кол-во жизней
  int speed;        //Текущая скорость ракеты
  cchar_t disp_char;	//Символ игрока
  _Bool shoot;
} player;

extern struct plasmaStruct{
  int y,x;
  int pair;
  cchar_t bg_char;
  cchar_t disp_char;
} plasma[5];

typedef enum {
  INTRO        = 0,
  MENU         = 1,
  ABOUT_GAME   = 2,
  ENTER_NAME   = 3,
  GAME         = 4,
  PAUSE        = 5,
  GAMEOVER     = 6,
  EXIT_GAME    = 7,
  RECORDS      = 8
} scene_type;

typedef enum {
  COLLISION_WALL,
  COLLISION_PLAYER,
  COLLISION_PLASMA
} collision_type;

extern wchar_t *player_frames[][3];
extern wchar_t healthbar[5];

extern int currentSpeed;
extern int currentFrame;

extern int speed_mm;
extern int record_mm;

extern _Bool staticDrawBG;
extern _Bool scrollBool;
extern _Bool pauseBool;

_Bool collision_Update(WINDOW *, collision_type);
_Bool returnBorders(WINDOW *);
_Bool returnObject(WINDOW *, int, int, const wchar_t *);
_Bool plasmaCollisions(WINDOW *);

void cursInit(void);
void cinit_pairs(void);

void initobjects(WINDOW *);
void initcchars(void);

int gameMenu(void);
void drawmenu(WINDOW *,int);

void scrollGameMap(WINDOW *, _Bool);
void drawGameBG(WINDOW *, _Bool);
void frwindow(WINDOW *, chtype);
void display_box(WINDOW *, int, int, char *, attr_t, int);
void display_GUI(WINDOW *);

void enterName(void);

void gameOver(WINDOW *);
void gamePause(WINDOW *, _Bool);

void gameOverBoxUI(WINDOW *window);
void pauseBoxUI(WINDOW *, _Bool);

void drawPlasma(WINDOW *);

void nraddcuscch(WINDOW *, cchar_t, wchar_t *, attr_t, int);

void addcuscch(WINDOW *, cchar_t, wchar_t *, attr_t, int);
void mvaddcuscchyx(WINDOW *, int, int, cchar_t, wchar_t *, attr_t, int);

void gameLoop(void);

int recordUpdate(void);

void recordsInit(void);
void recordsCompare(void);
void recordsSave(void);
void recordsBoxUI(WINDOW *);

void destroyObject(WINDOW *, int y, int x);

void pControls(WINDOW *, int);

void rocketshoot(WINDOW *);
void checkPlasma(WINDOW *);

int randomizer(int, int);
int msleep(int);
