#include "global.h"

#define MENUMAX 4

static int key,menuitem;
static const char mainmenu[] = GAME_TITLE;

void drawmenu(WINDOW *window,int item) {
  curs_set(0);
  nodelay(stdscr, FALSE);
  
  int c;
  
  char menu[MENUMAX][25] = {
    "НАЧАТЬ ИГРУ",
    "РЕКОРДЫ",
    "О ИГРЕ",
    "ВЫЙТИ ИЗ ИГРЫ",
  };
  
  for(c=0;c<MENUMAX;c++)
  {
    if( c==item )
      wattron(window,A_REVERSE | COLOR_PAIR(0));
    
    if(c!=1 || c!=2) {
      wattron(window,A_NORMAL | COLOR_PAIR(0));
    } else {
      wattron(window,A_DIM | COLOR_PAIR(0)); 
    }
    
    mvwaddstr(window,5+(c+1),5,menu[c]);
    
    if(c!=1 || c!=2) {
      wattroff(window,A_REVERSE | COLOR_PAIR(0));
    } else {
      wattroff(window,A_DIM | COLOR_PAIR(0));
    }
  }
}

int gameMenu(void){
  const int m_height = 12;
  const int m_width  = 24;
  
  clear();
  
  drawGameBG(stdscr, staticDrawBG);
  wnoutrefresh (stdscr);
  
  WINDOW *menuBox = newwin(m_height, m_width, (getmaxy(stdscr) / 2) - m_height / 2, (getmaxx(stdscr) / 2) - m_width / 2);
  WINDOW *title	= derwin(menuBox, 3, sizeof(mainmenu) + 5, 1, 3);
  
  wattron(title, COLOR_PAIR(COLOR_WHITE));
  box(title,0,0);
  wattroff(title, COLOR_PAIR(COLOR_WHITE));
  
  wattron(menuBox, COLOR_PAIR(COLOR_WHITE));
  box(menuBox,0,0);
  wattroff(menuBox, COLOR_PAIR(COLOR_WHITE));
  
  menuitem = 0;
  drawmenu(menuBox,menuitem);
  
  wattron(title, COLOR_PAIR(COLOR_WHITE));
  mvwaddstr(title, 1, 3, mainmenu);
  wattroff(title, COLOR_PAIR(COLOR_WHITE));
  //touchwin (menuBox);
  wnoutrefresh(menuBox);
  doupdate ();
  
  do {
    key = getch();
    switch(key)
    {
      case KEY_DOWN:
        menuitem++;
        if(menuitem > MENUMAX-1) menuitem = 0;
        break;
      case KEY_UP:
        menuitem--;
        if(menuitem < 0) menuitem = MENUMAX-1;
        break;
      default:
        continue;
    }
    drawmenu(menuBox,menuitem);
    wrefresh (menuBox);
  } while(key != '\n');
  
  delwin(title);
  delwin(menuBox);
  
  switch(menuitem){
    case 0:
      return ENTER_NAME;
      break;
    case 1:
      return RECORDS;
      break;
    case 2:
      return ABOUT_GAME;
      break;
    case 3:
      return EXIT_GAME;
      break;
  }
  
  nodelay(stdscr, TRUE);
  
  endwin();
  
  return menuitem;
}
