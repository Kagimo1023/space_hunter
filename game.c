#include <assert.h>
#include "global.h"

_Bool scrollBool;
_Bool pauseBool;

void gameLoop(void){
  int currentSpeed = GAME_SPEED;
  _Bool ex;
  record_mm = 0;

  const int gmwy = (getmaxy(stdscr) / 2) - CAMERA_HEIGHT / 2;
  const int gmwx = (getmaxx(stdscr) / 2) - CAMERA_WIDTH / 2;

  scrollBool = TRUE;
  pauseBool  = FALSE;

  curs_set(0);

  WINDOW *gmw = newwin(CAMERA_HEIGHT, CAMERA_WIDTH, gmwy, gmwx);
  WINDOW *title = newwin(3,CAMERA_WIDTH, getbegy(gmw) - 3, getbegx(gmw));
  WINDOW *tui = newwin(4,CAMERA_WIDTH, gmwy + getmaxy(gmw), getbegx(gmw));

  initobjects(gmw);

  scrollok(gmw, scrollBool);

  int ch;

  ex = FALSE;

  drawGameBG(gmw, staticDrawBG);
  box(title,0,0);
  mvwprintw(title,1, (CAMERA_WIDTH / 2) - strlen(GAME_TITLE) / 2, GAME_TITLE);
  mvwin(player.ply,getbegy(gmw) + player.y, getbegx(gmw) + player.x);

  while (!ex){
    pControls(gmw, ch = getch());
    collision_Update(gmw, COLLISION_WALL);
    scrollGameMap(gmw,scrollBool);
    box(gmw,0,0);
    if (plasma->y){
        drawPlasma(gmw);
        checkPlasma(gmw);
    }
    
    display_GUI(tui);

    touchwin (player.ply);

    wnoutrefresh(title);
    wnoutrefresh(gmw);

    wnoutrefresh(tui);
    wnoutrefresh(player.ply);
    doupdate ();

    usleep(currentSpeed);

    if (plasma->y){
      plasma->y--;
      drawPlasma(gmw);
      checkPlasma(gmw);
      wrefresh (gmw);
    }

    usleep(currentSpeed);
    
    recordUpdate();
    
    if(collision_Update(gmw,COLLISION_PLAYER)){
      flash();
      beep();
      
      --player.currentHP;
      currentSpeed = currentSpeed / 1.30;
      
      destroyObject(gmw,player.y - 1, player.x);
    }
    
    if(player.currentHP == 0)
      ex = TRUE;
  }

  gameOver(gmw);
  
  delwin(gmw);
  delwin(title);
  delwin(tui);

  refresh();

  return;
}

void rocketshoot (WINDOW *window){
  if (!plasma->y){
    plasma->x = player.x;
    plasma->y = player.y - 1;
    mvwin_wch (window, plasma->y, plasma->x, &plasma->bg_char);
  }
}

int recordUpdate(void){
  return record_mm = record_mm + player.speed;
}
