#include "global.h"

int main(void){
  setlocale(LC_ALL, "");
  srandom((unsigned)time(NULL));
  
  int choice = -1;
  
  cursInit();
  
  while(1){
    switch(gameMenu()){
      case ENTER_NAME:
        enterName();
        break;
      case RECORDS:
        recordsBoxUI(stdscr);
        break;
      case ABOUT_GAME:
        /*ЗДЕСЬ ДОЛЖЕН БЫТЬ ВЫЗОВ ФУНКЦИИ С ВЫВОДОМ ИНФОРМАЦИИ О ИГРЕ*/
        break;
      case EXIT_GAME:
        endwin();
        exit(0);
        break;
    }
  }
  
  endwin();
  
  return 0;
}
